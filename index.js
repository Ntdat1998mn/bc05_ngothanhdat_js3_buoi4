/** Bài tập chính
 *  Bài 1: Cho người dùng nhập vào 3 số nguyên.
 * Viết chương trình xuất 3 số theo thứ tự tăng dần
 *
 * Input: Nhập vào 3 bất kì
 *
 *Step:  1- Tạo giao diện để người dùng nhập vào, nơi để trả kết quả sau sự kiện và tạo event để thực hiện function
 *       2- Lấy 3 giá trị số từ HTML gán vào biến num1, num2, num3
 *       3- Tạo 1 biến lưu chuỗi cần sắp xếp (myArrangement)
 *       4- So sánh num1 với 2 biến còn lại nếu
 *          a- Nếu nhỏ nhất lưu ra đầu
 *            - So sánh Num2 và num3 để xếp chúng theo thứ tự tăng dần (số nào lớn đứng sau và số còn lại nằm kề trước)
 *          b- Nếu lớn nhất lưu ra cuối
 *            - So sánh Num2 và num3 để xếp chúng theo thứ tự tăng dần (số nào nhỏ đứng đầu và số còn lại nằm kề sau)
 *          c- Trường hợp khác sẽ lưu ở giữa
 *            - So sánh Num2 và num3 để xếp chúng (số nào lớn đứng trước tiên và số còn lại nằm sau cùng)
 *
 * Output: Xuất ra 1 chuỗi có giá trị tăng dần
 */
function arrangeNum() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  var myArrangement = "";
  if (num1 <= num2 && num1 <= num3) {
    myArrangement = `${num1}`;
    if (num2 <= num3) {
      myArrangement = `${myArrangement}, ${num2}, ${num3}`;
    } else {
      myArrangement = `${myArrangement}, ${num3}, ${num2}`;
    }
  } else if (num1 > num2 && num1 > num3) {
    myArrangement = `${num1}`;
    if (num2 <= num3) {
      myArrangement = `${num2}, ${num3}, ${myArrangement}`;
    } else {
      myArrangement = `${num3}, ${num2}, ${myArrangement}`;
    }
  } else {
    myArrangement = `${num1}`;
    if (num2 <= num3) {
      myArrangement = `${num2}, ${myArrangement}, ${num3}`;
    } else {
      myArrangement = `${num3}, ${myArrangement}, ${num2}`;
    }
  }
  document.getElementById("arrange").innerText = myArrangement;
}

/**
 * Bài 2: Chương trình "Chào hỏi
 *
 * Input: Chọn thành viên để chào thông qua các tuỳ chọn cho trước
 *
 * Step : 1- Tạo option để người dùng lựa chọn, nơi được trả kết quả và tạo event
 *        2- Lấy giá trị từ lựa chọn của của người dùng
 *        3- Viết lời chào cho từng value tuỳ chọn được lấy về
 *
 * Output: Gửi lời chào tương ứng với vai trò trong gia đình.
 *
 */
var sayGreeting = function () {
  var member = document.getElementById("member").value;
  if (member == "stranger") {
    document.getElementById("greeting").innerText = `Xin chào người lạ!`;
  } else {
    document.getElementById("greeting").innerText = `Xin chào ${member}!`;
  }
};

/**
 * Bài 3: Đếm số chẵn lẻ
 *
 * Input: Nhập vào 3 số bất kỳ
 *
 * Step: 1- Tạo nơi nhập xuất giữ liệu và event để lấy giá trị và xuất giá trị
 *       2- Lấy giá trị từ ô input được người dùng nhập vào
 *       3- Tạo 2 biến làm bộ đếm số chẵn lẻ
 *       4- Kiểm tra từng số có chia hết cho 2 hay không, nếu có thì số chẵn sẽ được cộng thêm 1, nếu không số lẻ sẽ được cộng thêm 1
 *       5- Trả kết quả
 * Output: Trả ra kết quả là có bao nhiêu số chẵn là bao nhiêu số lẻ trong các số nhập vào
 */
function countNum() {
  var even = 0,
    odd = 0;
  var num1_3 = document.getElementById("num1_3").value * 1;
  var num2_3 = document.getElementById("num2_3").value * 1;
  var num3_3 = document.getElementById("num3_3").value * 1;
  if (num1_3 % 2 == 0) {
    even++;
  } else {
    odd++;
  }
  if (num2_3 % 2 == 0) {
    even++;
  } else {
    odd++;
  }
  if (num3_3 % 2 == 0) {
    even++;
  } else {
    odd++;
  }
  document.getElementById(
    "result"
  ).innerText = `Ta có ${even} số chẵn và ${odd} số lẻ`;
}
/**
 * Bài 4: Đoán hình tam giác
 *
 * Input: Nhập vào độ dài của 3 cạnh tam giác
 *
 *Step: 1- Tạo nơi nhập xuất giữ liệu và event để lấy giá trị và xuất giá trị 
        2- Tạo biến lưu giá trị người dùng nhập vào
        3- Kiểm tra xem thông tin người dùng nhập vào có hợp lệ không (Xem có phải là tam giác không nếu không đúng thì thông báo với người dùng)
        4- Nếu đúng kiểm tra xem có phải tam giác đều  không nếu phải thì trả ra kết quả, nếu đúng kiểm tra tiếp 
        5- Kiểm tra xem phải tam giác cân không, nếu đúng thì trả kết quả nếu không thì tiếp tục
        6- Kiểm tra xem phải tam giác vuông không nếu đúng thì trả kết quả tam giác vuông, sai thì là tam giác thường

 * Output: Dự đoán tam giá gì( Chỉ xử lí các tam giác cơ bản như tam giác đều, cân, vuông)
 */
function guess() {
  var edge1 = document.getElementById("edge1").value * 1;
  var edge2 = document.getElementById("edge2").value * 1;
  var edge3 = document.getElementById("edge3").value * 1;
  var myTraingle = "";
  if (
    edge1 + edge2 <= edge3 ||
    edge1 + edge3 <= edge2 ||
    edge2 + edge3 <= edge1
  ) {
    alert("Đây không phải là tam giác");
  } else if (edge1 == edge2 && edge1 == edge3) {
    myTraingle = "đều";
  } else if (edge1 == edge2 || edge2 == edge3 || edge3 == edge1) {
    myTraingle = "cân";
  } else if (
    edge1 ** 2 + edge2 ** 2 == edge3 ** 2 ||
    edge1 ** 2 + edge3 ** 2 == edge2 ** 2 ||
    edge3 ** 2 + edge2 ** 2 == edge1 ** 2
  ) {
    myTraingle = "vuông";
  } else {
    myTraingle = "thường";
  }
  document.getElementById("resultGeuss").innerText = ` ${myTraingle}`;
}

/**Bài tập thêm */

/** 
 * Bài 5: Tính ngày tháng năm
 *
 * Input: Nhập ngày tháng năm bất ki
 * 
 * Step : I- Tạo giao diện cho người dùng nhập vào, nơi trả kết quả và sự kiện cho nút bấm cho từng yêu cầu của người dùng
          II- Tạo hàm để kiểm tra kết quả xem người dùng có nhập giá trị hợp lệ không testFuntion()
              1- Nếu người dùng nhập sai sẽ trả kết quả ra biến test là false và báo người dùng nhập lại
              2- Nếu "..............đúng................................"true
          III- Tạo hàm chứa hàm kiểm tra testFuntion() để tìm ngày hôm trước và sau
              1- Đối với hàm tìm ngày hôm trước findYesterday() khi giá trị = true thì thực hiện
                - Giảm ngày (date) đi 1 đơn vị và kiểm tra nếu ngày (date) bị giảm về 0 thì lùi tháng month về 1 đơn vị và cho ngày tương ứng với ngày tối đa tháng trước đó ( 
                  Phần này được chia thành nhóm tháng 1,3,5,7,8,10,12 tối đa 31; tháng 4,6,9,11 tối đa 30; tháng 2 thì 28 hoặc 29 tuỳ năm
                )
                - Nếu tháng month = 0 thì lùi năm về 1 đơn vị và cho tháng month = 12
                - Sau khi tìm được thì trả ra màn hình ngày/tháng/năm 
              2- Khi giá trị bằng = false thì trả ra màn hình =""
 * 
 * Output: Trả kết quả là ngày hôm trước hay hôm sau tuỳ yêu cầu của người dùng
 * 
 */

function testFuntion(date, month, year) {
  /* var date = parseInt(document.getElementById("date").value);
  var month = parseInt(document.getElementById("month").value);
  var year = parseInt(document.getElementById("year").value); */
  var test = true;
  if (isNaN(year)) {
    alert("Nhập năm không hợp lệ");
    test = false;
  } else if (month > 0 && month <= 12) {
    if (
      month == 1 ||
      month == 3 ||
      month == 5 ||
      month == 7 ||
      month == 8 ||
      month == 10 ||
      month == 12
    ) {
      if (date > 31 || date <= 0 || isNaN(date)) {
        test = false;
        alert("Ngày không hợp lệ");
      }
    } else if (month == 4 || month == 6 || month == 9 || month == 11) {
      if (date > 30 || date <= 0 || isNaN(date)) {
        test = false;
        alert("Ngày không hợp lệ");
      }
    } else if (month == 2) {
      if ((year % 4 == 0 && year % 100 !== 0) || year % 400 == 0) {
        if (date > 29 || isNaN(date)) {
          test = false;
          alert("Ngày không hợp lệ");
        }
      } else if (date > 28 || isNaN(date)) {
        test = false;
        alert("Ngày không hợp lệ");
      }
    }
  } else {
    test = false;
    alert("Tháng không hợp lệ");
  }
  return test;
}
function findYesterday() {
  var date = parseInt(document.getElementById("date").value);
  var month = parseInt(document.getElementById("month").value);
  var year = parseInt(document.getElementById("year").value);
  switch (testFuntion(date, month, year)) {
    case true:
      date--;
      if (date == 0) {
        month--;
        if (
          month == 1 ||
          month == 3 ||
          month == 5 ||
          month == 7 ||
          month == 8 ||
          month == 10 ||
          month == 12
        ) {
          date = 31;
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
          date = 30;
        } else if (month == 2) {
          if ((year % 4 == 0 && year % 100 !== 0) || year % 400 == 0) {
            date = 29;
          } else {
            date = 28;
          }
        } else {
          date = 31;
          month = 12;
          year--;
        }
      }
      document.getElementById(
        "result5"
      ).innerText = ` ${date}/${month}/${year}`;
      break;
    case false:
      document.getElementById("result5").innerText = "";
  }
}
function findTomorrow() {
  var date = parseInt(document.getElementById("date").value);
  var month = parseInt(document.getElementById("month").value);
  var year = parseInt(document.getElementById("year").value);
  if (testFuntion(date, month, year) == true) {
    date++;
    if (
      (date == 31 && (month == 4 || month == 6 || month == 9 || month == 11)) ||
      (date == 32 &&
        (month == 1 ||
          month == 3 ||
          month == 5 ||
          month == 7 ||
          month == 8 ||
          month == 10)) ||
      (month == 2 &&
        date == 30 &&
        ((year % 4 == 0 && year % 100 !== 0) || year % 400 == 0)) ||
      (month == 2 &&
        date == 29 &&
        !((year % 4 == 0 && year % 100 !== 0) || year % 400 == 0))
    ) {
      date = 1;
      month++;
    } else if (month == 12 && date == 32) {
      date = 1;
      month = 1;
      year++;
    }
    document.getElementById("result5").innerText = ` ${date}/${month}/${year}`;
  } else {
    document.getElementById("result5").innerText = "";
  }
}

/**
 * Bài 6: Tính ngày
 */

function testMonth(month, year) {
  if (isNaN(year)) {
    alert("Nhập năm không hợp lệ");
    return false;
  } else if (month > 0 && month < 13) {
    return true;
  } else {
    alert("Nhập tháng không hợp lệ");
    return false;
  }
}
function countDayOfMonth() {
  var month = parseInt(document.getElementById("month_6").value);
  var year = parseInt(document.getElementById("year_6").value);
  switch (testMonth(month, year)) {
    case true:
      if (month == 4 || month == 6 || month == 9 || month == 11) {
        return 30;
      } else if (
        month == 1 ||
        month == 3 ||
        month == 5 ||
        month == 7 ||
        month == 8 ||
        month == 10 ||
        month == 12
      ) {
        return 31;
      } else if ((year % 4 == 0 && year % 100 !== 0) || year % 400 == 0) {
        return 29;
      } else {
        return 28;
      }
    default:
      return "";
  }
}
function findDay() {
  document.getElementById("result6").innerText = countDayOfMonth();
}

/**
 * Bài 7: Đọc số
 */

function readNumber() {
  var number = document.getElementById("number").value * 1;
  if (number >= 100 && number < 1000) {
    var unit = Math.floor(number % 10),
      ten = Math.floor(Math.floor(number % 100) / 10),
      hundred = Math.floor(number / 100);
    var myString = "";
    switch (hundred) {
      case 1:
        myString = "Một Trăm";
        break;
      case 2:
        myString = "Hai Trăm";
        break;
      case 3:
        myString = "Ba Trăm";
        break;
      case 4:
        myString = "Bốn Trăm";
        break;
      case 5:
        myString = "Năm Trăm";
        break;
      case 6:
        myString = "Sáu Trăm";
        break;
      case 7:
        myString = "Bảy Trăm";
        break;
      case 8:
        myString = "Tám Trăm";
        break;
      case 9:
        myString = "Chín Trăm";
        break;
    }
    switch (ten) {
      case 1:
        myString = `${myString} Mười`;
        break;
      case 2:
        myString = `${myString} Hai Mươi`;
        break;
      case 3:
        myString = `${myString} Ba Mươi;`;
        break;
      case 4:
        myString = `${myString} Bốn Mươi`;
        break;
      case 5:
        myString = `${myString} Năm Mươi`;
        break;
      case 6:
        myString = `${myString} Sáu Mươi`;
        break;
      case 7:
        myString = `${myString} Bảy Mươi`;
        break;
      case 8:
        myString = `${myString} Tám Mươi`;
        break;
      case 9:
        myString = `${myString} Chín Mươi`;
        break;
      case 0:
        if (unit == 0) {
          myString = myString;
        } else {
          myString = `${myString} Linh`;
        }
        break;
    }
    switch (unit) {
      case 1:
        if (ten == 1) {
          myString = `${myString} Một`;
        } else {
          myString = `${myString} Mốt`;
        }
        break;
      case 2:
        myString = `${myString} Hai`;
        break;
      case 3:
        myString = `${myString} Ba`;
        break;
      case 4:
        myString = `${myString} Bốn`;
        break;
      case 5:
        myString = `${myString} Năm`;
        break;
      case 6:
        myString = `${myString} Sáu`;
        break;
      case 7:
        myString = `${myString} Bảy`;
        break;
      case 8:
        myString = `${myString} Tám`;
        break;
      case 9:
        myString = `${myString} Chín`;
        break;
    }
    document.getElementById("writeNumber").innerText = myString;
  } else {
    alert("Số không hợp lệ");
    return (document.getElementById("writeNumber").innerText = "");
  }
}

/**
 * Bài 8: Tìm sinh viên xa trường nhất
 */
function testFuntion8(
  name1,
  name2,
  name3,
  x1,
  x2,
  x3,
  xOfSchool,
  y1,
  y2,
  y3,
  yOfSchool
) {
  if (
    isNaN(x1) ||
    isNaN(x2) ||
    isNaN(x3) ||
    isNaN(xOfSchool) ||
    isNaN(y1) ||
    isNaN(y2) ||
    isNaN(y3) ||
    isNaN(yOfSchool) ||
    name1 == "" ||
    name2 == "" ||
    name3 == ""
  ) {
    alert("Dữ liệu chưa hợp lệ");
    return false;
  } else {
    return true;
  }
}

function findStudent() {
  var name1 = document.getElementById("name1").value;
  var name2 = document.getElementById("name2").value;
  var name3 = document.getElementById("name3").value;

  var x1 = parseFloat(document.getElementById("x1").value);
  var x2 = parseFloat(document.getElementById("x2").value);
  var x3 = parseFloat(document.getElementById("x3").value);
  var xOfSchool = parseFloat(document.getElementById("xOfSchool").value);

  var y1 = parseFloat(document.getElementById("y1").value);
  var y2 = parseFloat(document.getElementById("y2").value);
  var y3 = parseFloat(document.getElementById("y3").value);
  var yOfSchool = parseFloat(document.getElementById("yOfSchool").value);

  switch (
    testFuntion8(
      name1,
      name2,
      name3,
      x1,
      x2,
      x3,
      xOfSchool,
      y1,
      y2,
      y3,
      yOfSchool
    )
  ) {
    case true:
      var distanceSchoolToStudent1 = Math.sqrt(
        (x1 - xOfSchool) ** 2 + (y1 - yOfSchool) ** 2
      );
      var distanceSchoolToStudent2 = Math.sqrt(
        (x2 - xOfSchool) ** 2 + (y2 - yOfSchool) ** 2
      );
      var distanceSchoolToStudent3 = Math.sqrt(
        (x3 - xOfSchool) ** 2 + (y3 - yOfSchool) ** 2
      );
      if (
        distanceSchoolToStudent1 > distanceSchoolToStudent2 &&
        distanceSchoolToStudent1 > distanceSchoolToStudent3
      ) {
        return (document.getElementById(
          "student"
        ).innerText = ` ${name1} xa trường nhất`);
      } else if (
        distanceSchoolToStudent2 > distanceSchoolToStudent1 &&
        distanceSchoolToStudent2 > distanceSchoolToStudent3
      ) {
        return (document.getElementById(
          "student"
        ).innerText = ` ${name2} xa trường nhất`);
      } else if (
        distanceSchoolToStudent3 > distanceSchoolToStudent1 &&
        distanceSchoolToStudent3 > distanceSchoolToStudent2
      ) {
        return (document.getElementById(
          "student"
        ).innerText = ` ${name3} xa trường nhất`);
      } else if (
        distanceSchoolToStudent1 == distanceSchoolToStudent2 &&
        distanceSchoolToStudent2 == distanceSchoolToStudent3
      ) {
        return (document.getElementById(
          "student"
        ).innerText = ` ${name1}, ${name2}, ${name3} xa như nhau và xa nhất`);
      } else if (
        distanceSchoolToStudent1 == distanceSchoolToStudent2 &&
        distanceSchoolToStudent2 > distanceSchoolToStudent3
      ) {
        return (document.getElementById(
          "student"
        ).innerText = ` ${name1}, ${name2} xa như nhau và xa nhất`);
      } else if (
        distanceSchoolToStudent3 == distanceSchoolToStudent2 &&
        distanceSchoolToStudent2 > distanceSchoolToStudent1
      ) {
        return (document.getElementById(
          "student"
        ).innerText = ` ${name2}, ${name3} xa như nhau và xa nhất`);
      } else if (
        distanceSchoolToStudent3 == distanceSchoolToStudent1 &&
        distanceSchoolToStudent1 > distanceSchoolToStudent2
      ) {
        return (document.getElementById(
          "student"
        ).innerText = ` ${name1}, ${name3} xa như nhau và xa nhất`);
      }
      break;
    case false:
      return (document.getElementById("student").innerText = "");
  }
}
